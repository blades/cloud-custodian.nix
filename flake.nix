{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    dream2nix = {
      url = "github:nix-community/dream2nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    flake-utils.url = "github:numtide/flake-utils";
    cloud-custodian = {
      flake = false;
      url = "github:cloud-custodian/cloud-custodian?tag=0.9.24.0";
    };
  };

  outputs = inputs@{ self, nixpkgs, dream2nix, flake-utils, cloud-custodian, ... }:
    dream2nix.lib.makeFlakeOutputs {
      source = cloud-custodian;
      config.projectRoot = ./.;
      systems = flake-utils.lib.defaultSystems;
      projects = ./projects.toml;
    };
}
